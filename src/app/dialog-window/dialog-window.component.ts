import { SnackBarComponent } from "./../snack-bar/snack-bar.component";
import { XlsxService } from "./../services/xlsx.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Inject } from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material/dialog";

export interface DialogData {
  sheetNameToSave: string;
  tableEl: HTMLCollection;
}

@Component({
  selector: "app-dialog-window",
  templateUrl: "./dialog-window.component.html",
  styleUrls: ["./dialog-window.component.css"]
})
export class DialogWindowComponent {

  public sheetNameToSave: string;

  constructor(public dialog: MatDialog) {}

  openDialog(sheetNameToSave, tableEl): void {
    const dialogRef = this.dialog.open(DialogBodyDialog, {
      width: "300px",
      data: { sheetNameToSave: sheetNameToSave, tableEl: tableEl }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog was closed");
    });
  }
}

@Component({
  selector: "dialog-body-dialog",
  templateUrl: "dialog-body-dialog.html"
})
export class DialogBodyDialog {
  @ViewChild(SnackBarComponent, { static: true })
  snackBar;
  constructor(
    public dialogRef: MatDialogRef<DialogWindowComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public xlsxServise: XlsxService
  ) {}
  onOkClick(sheetNameToSave: string, tableEl: HTMLCollection) {
    this.xlsxServise.onSaveSheet(sheetNameToSave, tableEl, this.snackBar);
    this.dialogRef.close();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
