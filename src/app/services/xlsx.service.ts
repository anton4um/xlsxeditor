import { MatSnackBar } from "@angular/material/snack-bar";
import { SnackBarComponent } from "./../snack-bar/snack-bar.component";
import { Injectable, OnInit } from "@angular/core";
import * as XLSX from "xlsx";
import * as $ from "jquery";
import { Subject } from "rxjs";
import { resolve } from "url";
// import * as snackBar from SnackBarComponent

@Injectable({ providedIn: "root" })
export class XlsxService implements OnInit {
  public willDownload = false;
  workBook = null;
  workSheetName: string = null;
  workSheetNames: string[] = null;
  workSheet: XLSX.WorkSheet;
  public fileType = null;
  willDownloadObserver = new Subject();
  fileTypeObserver = new Subject();
  snackBar: SnackBarComponent;
  tableSomethingSelectedObs = new Subject();
  ngOnInit() {}

  workBookToXlsx() {
    XLSX.writeFile(this.workBook, "ExportedDataInXlsx.xlsx", {
      bookType: "xlsx",
      bookSST: false,
      type: "base64"
    });
  }

  async currentSheetToCsv(selectedSheet: string) {
    let downloadLinkWasSet;
    this.fileType = "ExportedDataInCsv.csv";
    this.fileTypeObserver.next(this.fileType);
    // this.makeCleanAttrElDataBeforeOutput();
    let csvData = null;
    csvData = XLSX.utils.sheet_to_csv(this.workBook.Sheets[selectedSheet], {});
    // $("#output-exported-data").html(csvData.slice(0, 300).concat("..."));
    await this.setDownloadFile(csvData, "ExportedDataInCsv.csv").then(
      result => {
        // console.log("\nresult of Link Setting\n", result);
        if (result.href && result.download) {
          downloadLinkWasSet = { linkIsNotSetCSV: null };
        } else {
          downloadLinkWasSet = { linkIsNotSetCSV: true };
        }
        // console.log("\n In Promise returnedResult CSV: \n", downloadLinkWasSet);
        return downloadLinkWasSet;
      }
    );
    // console.log("\nOut Of Promise returnedResult CSV: \n", downloadLinkWasSet);
    return downloadLinkWasSet;
  }

  async currentSheetToJson(selectedSheet: string) {
    let downloadLinkWasSet;
    this.fileType = "ExportedDataInJson.json";
    this.fileTypeObserver.next(this.fileType);
    // this.makeCleanAttrElDataBeforeOutput();
    let jsonData = XLSX.utils.sheet_to_json(
      this.workBook.Sheets[selectedSheet]
    );
    let dataString = JSON.stringify(jsonData);
    // $("#output-exported-data").html(dataString.slice(0, 300).concat("..."));
    await this.setDownloadFile(dataString, "ExportedDataInJson.json").then(
      result => {
        if (result.href && result.download) {
          downloadLinkWasSet = { linkIsNotSetJSON: null };
        } else {
          downloadLinkWasSet = { linkIsNotSetJSON: true };
        }
        // console.log("\n I Promise returnedResult JSON: \n", downloadLinkWasSet);
      }
    );
    console.log(
      "\nOut Of Promise downloadLinkWasSet JSON: \n",
      downloadLinkWasSet
    );
    return downloadLinkWasSet;
  }
  setDownloadFile(data: string, fileType: string) {
    this.willDownload = true;
    this.willDownloadObserver.next(this.willDownload);
    let promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        $("#download").attr(
          "href",
          `data:text/json;charset=utf-8,${encodeURIComponent(data)}`
        );
        $("#download").attr("download", fileType);
        resolve({
          href: $("#download").attr("href"),
          download: $("#download").attr("download")
        });
      }, 1000);
    });
    return promise;
  }
  onSaveSheet(saveSelectedSheet: string, tableEl: Object, snackBar: any) {
    $("table tr > td:nth-child(1), table tr > th:nth-child(1)").hide();
    $("table tr:nth-child(1)").hide();
    this.workBook.Sheets[saveSelectedSheet] = XLSX.utils.table_to_sheet(
      tableEl,
      { display: true }
    );
    console.log("\nthis.workBook Sheets: \n", this.workBook);
    $("table tr > td:nth-child(1), table tr > th:nth-child(1)").show();
    $("table tr:nth-child(1)").show();
    snackBar.openSnackBar(
      'Changes Saved To "' + saveSelectedSheet + '"',
      "Successfully!!!"
    );
  }
  onReadFile(file: File) {
    const reader = new FileReader();
    let promise = new Promise((resolve, reject) => {
      reader.onload = event => {
        const data = reader.result;
        this.workBook = XLSX.read(data, { type: "binary" });
        this.workSheetNames = this.workBook.SheetNames;
        resolve(this.workSheetNames);
        // console.log("\nwork book: \n", this.workBook);
      };
    });
    reader.readAsBinaryString(file);
    return promise;
  }

  onChangeCheckDetect(tableModified, dialogWindow, sheetNameToSave, tableEl) {
    if (tableModified) {
      dialogWindow.openDialog(sheetNameToSave, tableEl);
    }
  }
  //   onDeletSelected() {
  //     $("[selected]").remove();
  //     let tbody = document.getElementsByTagName("tbody")[0];
  //     // reindexing columns
  //     for (let i = 0; i < tbody.rows[0].cells.length; i++) {
  //       $("[name=checkbox-col]:eq(" + i + ")").attr("index", i);
  //     }
  //     //reindexing rows
  //     for (let i = 0; i < tbody.rows.length; i++) {
  //       $("[name=checkbox-row]:eq(" + i + ")").attr("index", i + 1);
  //     }
  //   }

  onDeleteSelected() {
    $("[selected]").remove();
    let tbody = document.getElementsByTagName("tbody")[0];

    // reindexing columns
    for (let i = 0; i < tbody.rows[0].cells.length; i++) {
      $("[name=checkbox-col]:eq(" + i + ")").attr("index", i);
    }
    //reindexing rows
    for (let i = 0; i < tbody.rows.length; i++) {
      $("[name=checkbox-row]:eq(" + i + ")").attr("index", i + 1);
    }
  }

  sheetToHTML(sheetName) {
    let workSheet = this.workBook.Sheets[sheetName];
    let dataHTML = XLSX.utils.sheet_to_html(workSheet, { editable: true });
    return dataHTML;
  }

  uncheckAll() {
    $("input[type='checkbox']:checked").each((elIndex, elem) => {
      if ($(elem).attr("name") === "checkbox-row") {
        $(elem)
          .parent()
          .parent()
          .attr("style", "background-color: transperent;")
          .removeAttr("selected");
          $(elem).prop("checked", "");
      }
      if ($(elem).attr("name") === "checkbox-col") {
        let index = +$(elem).attr("index") + 1;
        console.log("\nelement index: \n", index);
        $(
          "table tr > td:nth-child(" +
            index +
            "), table tr > th:nth-child(" +
            index +
            ")"
        )
          .attr("style", "background-color: transperent;")
          .removeAttr("selected");
          $(elem).prop("checked", "");

      }
      this.tableSomethingSelectedObs.next(false);
    });
  }

  insertColumn() {
    let tbody = $("tbody")[0];
    let index = null;
    !(+$("th[selected] input").attr("index") + 1)
      ? (index = -1)
      : (index = +$("th[selected] input").attr("index") + 1);
    let newCheckBox = tbody.rows[0].insertCell(index);
    $(newCheckBox).replaceWith(
      "<th><input type='checkbox' name='checkbox-col' checked='true'></th>"
    );
    for (var i = 1; i < tbody.rows.length; i++) {
      const newCell = tbody.rows[i].insertCell(index);
      $(newCell)
        .attr("style", "background-color: orange")
        .html("<span contenteditable='true'></span>");
    }
    //reindexing columns
    for (let i = 0; i < tbody.rows[0].cells.length; i++) {
      const indexedCheckbox = $("[name=checkbox-col]:eq(" + i + ")").attr(
        "index",
        i
      );
    }
    this.removeChangeEventListener();
    this.addChangeEventListenerAndSelectLogic();
  }

  insertRow() {
    const table = $("table")[0];
    let selector = null;
    let index = null;
    if (!(+$("tr[selected] input").attr("index") + 1)) {
      index = -1;
      selector = "table tr td";
    } else {
      index = +$("tr[selected] input").attr("index") + 1;
      selector = "tr[selected] td";
    }
    const newRow = table.insertRow(index);
    // slice is needed if index = -1, to not copy all td in the table;
    const newRowContent = $(selector)
      .slice(0, table.rows[0].cells.length)
      .clone()
      .html("<span contenteditable='true'></span>");
    console.log("\nnew row content after clone: \n", newRowContent);
    $(newRowContent)
      .first()
      .html("<input type='checkbox' checked='true' name='checkbox-row'>");
    console.log("\nnew row content after add checkbox: \n", newRowContent);
    $(newRow)
      .html(newRowContent)
      .attr("style", "background-color: orange");
    // reindexing row checkboxes
    for (let i = 0; i < table.rows.length; i++) {
      $("[name=checkbox-row]:eq(" + i + ")").attr("index", i + 1);
    }
    this.removeChangeEventListener();
    this.addChangeEventListenerAndSelectLogic();
  }

  removeChangeEventListener() {
    $("input[type=checkbox]").unbind();
    // console.log("\nEvent Listeners Was Removed\n");
  }
  addChangeEventListenerAndSelectLogic() {
    const self = this;
    console.log("\nEvent Listener Was Added\n");
    $("input[type=checkbox]").change(function(event) {
      let index = +$(this).attr("index") + 1;
      console.log("\nCalculated index of selected checkbox: \n", index);
      if (
        $(this).prop("checked") === true &&
        $(this).attr("name") === "checkbox-col"
      ) {
        $(
          "table tr > td:nth-child(" +
            index +
            "), table tr > th:nth-child(" +
            index +
            ")"
        )
          .attr("style", "background-color:lightgray;")
          .attr("class", "text-muted")
          .attr("selected", "");
        self.tableSomethingSelectedObs.next(true);
      } else if (
        $(this).prop("checked") === false &&
        $(this).attr("name") === "checkbox-col"
      ) {
        $(
          "table tr > td:nth-child(" +
            index +
            "), table tr > th:nth-child(" +
            index +
            ")"
        )
          .attr("style", "background-color: transperent;")
          .attr("class", "")
          .removeAttr("selected", "");
        self.tableSomethingSelectedObs.next(false);
      }
      if (
        $(this).prop("checked") === true &&
        $(this).attr("name") === "checkbox-row"
      ) {
        $(this)
          .parent()
          .parent()
          .attr("style", "background-color:lightgray;")
          .attr("class", "text-muted")
          .attr("selected", "");
        self.tableSomethingSelectedObs.next(true);
      } else if (
        $(this).prop("checked") === false &&
        $(this).attr("name") === "checkbox-row"
      ) {
        // $("table tr:nth-child(" + index + ")")
        $(this)
          .parent()
          .parent()
          .attr("style", "background-color: transperent;")
          .attr("class", "")
          .removeAttr("selected", "");
        self.tableSomethingSelectedObs.next(false);
      }
    });
  }
}
