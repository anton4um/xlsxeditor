import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XlsxEditorComponent } from './xlsx-editor.component';

describe('XlsxEditorComponent', () => {
  let component: XlsxEditorComponent;
  let fixture: ComponentFixture<XlsxEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XlsxEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XlsxEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
