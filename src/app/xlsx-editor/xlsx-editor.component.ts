import { MatSnackBar } from "@angular/material/snack-bar";
import { DialogWindowComponent } from "./../dialog-window/dialog-window.component";
import { XlsxService } from "./../services/xlsx.service";
import { SnackBarComponent } from "./../snack-bar/snack-bar.component";
import {
  Component,
  OnInit,
  AfterViewInit,
  Renderer2,
  ViewChild,
  ElementRef
} from "@angular/core";
import * as XLSX from "xlsx";
import * as $ from "jquery";

@Component({
  selector: "app-xlsx-editor",
  templateUrl: "./xlsx-editor.component.html",
  styleUrls: ["./xlsx-editor.component.css"]
})
export class XlsxEditorComponent implements OnInit, AfterViewInit {
  @ViewChild(SnackBarComponent, { static: true })
  snackBar: any;
  @ViewChild(DialogWindowComponent, { static: true })
  dialogComponent: any;
  constructor(private renderer: Renderer2, private xlsxService: XlsxService) {}
  ngOnInit() {
    this.xlsxService.willDownloadObserver.subscribe((data: boolean) => {
      this.willDownload = data;
    });
    this.xlsxService.fileTypeObserver.subscribe((data: string) => {
      this.fileType = data;
    });
    this.xlsxService.tableSomethingSelectedObs.subscribe((data: boolean)=>{
      this.tableSomethingSelected = data;
    });
  }
  ngAfterViewInit() {}
  tableEl: HTMLElement;
  workSheetNames: any = null;
  selectedSheet: string = null;
  tableModified: boolean = false;
  tableSomethingSelected: boolean = false;
  willDownload = false;
  fileType;

  workBookToXlsx() {
    this.xlsxService.workBookToXlsx();
  }
  currentSheetToCsv(selectedSheet) {
    this.xlsxService.currentSheetToCsv(selectedSheet).then(result => {
      console.log("\nResult from service CSV: \n", result);
      if (result.linkIsNotSetCSV === null) {
        this.snackBar.openSnackBar(
          "Export Data To CSV Format Was Secceed",
          "Operation Successful!!!"
        );
      } else {
        this.snackBar.openSnackBar(
          "Some Problem Happened In Creating Download Link CSV File",
          "Operation Fail!!!"
        );
      }
    });
  }
  currentSheetToJson(currentSheet) {
    this.xlsxService.currentSheetToJson(currentSheet).then(result => {
      console.log("\nResult from service JSON: \n", result);
      if (result.linkIsNotSetJSON === null) {
        this.snackBar.openSnackBar(
          "Export Data To JSON Format Was Secceed",
          "Operation Successful!!!"
        );
      } else {
        this.snackBar.openSnackBar(
          "Some Problem Happened In Creating Download Link JSON File",
          "Operation Fail!!!"
        );
      }
    });
  }

  onChangeCheckDetect() {
    this.xlsxService.onChangeCheckDetect(
      this.tableModified,
      this.dialogComponent,
      this.selectedSheet,
      this.tableEl
    );
  }

  onSaveSheet(saveSelectedSheet) {
    this.tableModified = false;
    this.xlsxService.onSaveSheet(
      saveSelectedSheet,
      this.tableEl,
      this.snackBar
    );
  }

  onDeleteSelected() {
    this.xlsxService.onDeleteSelected();
    // this.tableSomethingSelected = false;
    this.xlsxService.tableSomethingSelectedObs.next(false);
  }

  onSheetSelect(sheetName) {
    this.willDownload = false;
    this.xlsxService.willDownloadObserver.next(this.willDownload);
    this.tableModified = false;
    // this.tableSomethingSelected = false;
    this.xlsxService.tableSomethingSelectedObs.next(false);
    this.selectedSheet = sheetName;
    let dataHTML = this.xlsxService.sheetToHTML(sheetName);
    // console.log("\ndataHTML: \n", dataHTML,
    // );
    document.getElementById("output").innerHTML = dataHTML;
    this.tableEl = document.getElementsByTagName("table")[0];
    this.renderer.addClass(this.tableEl, "table");
    this.renderer.addClass(this.tableEl, "table-hover");
    this.renderer.addClass(this.tableEl, "table-striped");
    this.renderer.addClass(this.tableEl, "table-responsive");
    this.renderer.addClass(this.tableEl, "table-sm");
    this.renderer.addClass(this.tableEl, "table-bordered");
    let tbody = document.getElementsByTagName("tbody")[0];
    let newCheckBox;
    let newCheckBoxCell;
    let newTh;
    let newTd;
    let cellsCol = tbody.rows[0].cells.length;
    let newRow = tbody.insertRow(0);

    // add checkboxes for columns of table
    if (tbody != null) {
      for (let i = 0; i <= cellsCol; i++) {
        newTh = document.createElement("th");
        newCheckBoxCell = document.createElement("input");
        newCheckBoxCell.type = "checkbox";
        // newCheckBoxCell.checked = "true";
        newCheckBoxCell.name = "checkbox-col";
        newTh.appendChild(newCheckBoxCell);
        newRow.appendChild(newTh);
        this.renderer.setAttribute(newCheckBoxCell, "index", i.toString());
      }
      let newButton = $(
        "<button class='btn btn-warning btn-sm'>CE</button>"
      );
      $(newRow.childNodes[0]).append(newButton);
      //hide checkbox with index 0 in row 0 in cell 0;
      console.log("\nbutton select: \n", $("button, .btn, .btn-warning, .btn-sm"));
      $(".btn").click(()=>{
        this.uncheckAll();
      })
      this.renderer.setStyle(
        newRow.childNodes[0].firstChild,
        "display",
        "none"
      );
    }

    // add checkboxes for rows of table
    if (tbody != null) {
      for (let i = 1; i <= tbody.childNodes.length - 1; i++) {
        newCheckBox = document.createElement("input");
        newCheckBox.type = "checkbox";
        newCheckBox.name = "checkbox-row";
        newTd = document.createElement("td");
        newTd.appendChild(newCheckBox);
        this.renderer.setAttribute(newCheckBox, "index", i.toString());
        tbody.childNodes[i].insertBefore(newTd, tbody.childNodes[i].firstChild);
      }
    }
    let self = this;
    this.xlsxService.removeChangeEventListener();
    this.xlsxService.addChangeEventListenerAndSelectLogic();
    $("table").change(() => {
      this.tableModified = true;
      // console.log("\ntableModified: \n", this.tableModified);
      if ($("[type=checkbox]:checked").length != 0) {
        this.tableSomethingSelected = true;
      }
    });
    $("span").keyup(() => {
      this.tableModified = true;
      console.log("\nCell Modified: \n", this.tableModified);
    });
  }

  onFileChange(ev) {
    const file = ev.target.files[0];
    this.xlsxService.onReadFile(file).then(result => {
      // console.log("\nPromise has returned: \n", result);
      this.workSheetNames = result;
      this.onSheetSelect(this.workSheetNames[0]);
    });
  }

  insertColumn() {
    this.xlsxService.insertColumn();
  }

  insertRow() {
    this.xlsxService.insertRow();
  }

  uncheckAll(){
    this.xlsxService.uncheckAll();
  }
}
