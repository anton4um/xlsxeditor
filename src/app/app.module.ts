import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { XlsxEditorComponent } from "./xlsx-editor/xlsx-editor.component";

import { MatSelectModule } from "@angular/material/select";
import { MatFormFieldModule, MatDialogModule } from "@angular/material";
import { MatCheckboxModule } from "@angular/material";
import {
  SnackBarComponent,
} from "./snack-bar/snack-bar.component";
import { MatSnackBarModule } from "@angular/material";
import { MatButtonModule } from "@angular/material";
import { DialogWindowComponent, DialogBodyDialog } from './dialog-window/dialog-window.component';
@NgModule({
  declarations: [AppComponent, XlsxEditorComponent, SnackBarComponent, DialogWindowComponent, DialogBodyDialog],
  entryComponents: [SnackBarComponent, DialogWindowComponent, DialogBodyDialog],
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
